import {Select} from './select';
import {DEFAULT} from "../error-labels";
import i18next from "i18next";
import {getName} from "../utils/getName";

export class StateSelect extends Select {

    isValid() {
        let value = this.model.get(this.full_name);

        if (/^[a-zA-Z]{2}$/.test(value)) {
            return true;
        }

        this.setValidationErrorToBox(DEFAULT);
        return false;
    }

    setValidationErrorToBox(errorType, replaceKeys = {}){
        let message = i18next.t(getName(this.element) + '.' + errorType, replaceKeys);
        let childNodes = this.element.parentNode.childNodes;
        let errorBox = null;

        for (let i = 0; i < childNodes.length; i++) {
            if (childNodes[i].className == "error-text") {
                errorBox = childNodes[i];
                break;
            }
        }

        errorBox.innerHTML = message;
    }
}
