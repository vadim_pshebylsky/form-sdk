import {Input} from './input';
import {FORM_NAME} from '../constants';
import {CardExpYear} from './card-exp-year';

export class CardExpMonth extends Input {
    isValid() {
        const expireYearFullName = `${FORM_NAME}.card_exp_year`;
        let expire_year = CardExpYear.prepareFormatValue(
            this.model.get(expireYearFullName)
        );
        const expire_month = this.element.value;

        if (expire_month.length === 0 || expire_month.length === 1 ) return false;

        if (expire_year.length === 0) {
            expire_year = (new Date()).getFullYear() + 1;
        }

        const expire_date = Date.parse([expire_month, '01', expire_year].join('/'));
        var cur_date = new Date();

        cur_date.setDate(1);
        cur_date.setHours(0);
        cur_date.setMinutes(0);
        cur_date.setSeconds(0);
        cur_date.setMilliseconds(0);


        if (expire_date >= cur_date.getTime()) {
            let expireYearComponent = this.getComponentByFullName(expireYearFullName);
            if (expireYearComponent && expireYearComponent.element.value.length > 0) {
                expireYearComponent.setValidationMark(true, true);
            }

            return true;
        }

        return false;
    }

    onBlur(event) {
        this.setDirty();
        if (this.element.value.length === 1) {
            this.element.value = "0" + this.element.value;
            this.setValidationMark(this.isValid(), true)
        }

    }

    getValue() {
        if (this.element.value.length === 1) {
            return "0" + this.element.value;
        }
        return this.element.value
    }

}
