import {Input} from './input';
import {FORM_NAME} from '../constants';
import {DEFAULT} from "../error-labels";

export class CardExpDate extends Input {
    constructor(...args) {
        super(...args);

        this.element.addEventListener('blur', this.onBlur.bind(this));
        this.element.addEventListener('keyup', e => {
            // backspace, arrows (up, down, left, right)
            const skipElements = [8, 37, 38, 39, 40];
            if (skipElements.includes(e.which)) return;
            const length = e.target.value.length;
            e.target.setSelectionRange(length, length);
        })

    }

    intercept(value) {
        this.model.set(this.full_name, value);
        this.dirty = true;
        this.setValidationMark(this.isValid());
        return this.prepareFormatValue(value, false);
    }

    onBlur(event) {
        this.setDirty();
        const expiryDate = this.model.get(this.full_name);
        this.element.value = this.prepareFormatValue(expiryDate, true);
    }

    prepareFormat() {
        const expiryDate = this.model.get(this.full_name);
        this.element.value = this.prepareFormatValue(expiryDate, false);
    }

    isValid() {
        const date_value = this.prepareFormatValue(this.element.value, false);
        const possibleLengthsMonth = [1, 2];
        const possibleLengthsYear = [2, 4];
        let dates = date_value.split('/');
        let month = dates[0];
        let year = dates[1];

        if (!month || !year) {
            return false;
        }

        if (!possibleLengthsMonth.includes(month.length) || !possibleLengthsYear.includes(year.length)){
            return false;
        }

        if (parseInt(month) > 12) return false;

        const expire_date = Date.parse([month, '01', this.prepareYearFormatValue(year)].join('/'));
        let cur_date = new Date();
        cur_date.setDate(1);
        cur_date.setHours(0);
        cur_date.setMinutes(0);
        cur_date.setSeconds(0);
        cur_date.setMilliseconds(0);

        if (expire_date >= cur_date.getTime()) {
            let fullYearValue = this.prepareYearFormatValue(dates[1]);
            if (/^[2]\d{3}$/.test(fullYearValue) === false) return false;

            this.model.set(`${FORM_NAME}.card_exp_month`, dates[0]);
            this.model.set(`${FORM_NAME}.card_exp_year`, fullYearValue);
        }

        if (expire_date >= cur_date.getTime()) {
            return true;
        }

        this.setValidationErrorToBox(DEFAULT);

        return false;
    }

    prepareFormatValue(expDate, isBlur) {
        if (expDate === undefined) {
            return '';
        }
        const dates = expDate.split('/');
        let month = dates[0] ? dates[0] : '';
        let year = dates[1] ? dates[1] : '';

        if (month === '' && year === '') {
            return '';
        }

        let monthDigits = [];
        if (!month && month !== '0') {
            month = '';
        } else {
            monthDigits = month.split('');
            if (monthDigits[0] !== '0' && parseInt(month) > 12) {
                if (year) {
                    year = monthDigits[1] + year;
                } else {
                    year = monthDigits[1];
                }
                month = monthDigits[0];
            }
            if (isBlur && monthDigits[0] !== '0' && parseInt(month) < 10) {
                month = '0' + month;
            }
            if (monthDigits[1] === '0' && monthDigits[0] === '0') {
                month = monthDigits[0];
            }
        }

        if (!year) {
            year = '';
        } else if (year.length > 4) {
            year = year.substring(0, 4);
        }

        if ((!month && month !== '0' && month !== ' ') && !year) {
            return '';
        }

        if ((month === '1' || month === '0') && !year) {
            return month;
        }

        return '' + month + '/' + year;
    }

    prepareYearFormatValue(expire_year) {
        const format = '201';
        if (expire_year.length < 4 && expire_year.length > 0) {
            var missingNumber = 4 - expire_year.length;
            expire_year = format.slice(0, missingNumber) + expire_year;
        }
        return expire_year;
    }
}
