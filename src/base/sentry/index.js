import {loadScript} from "../utils/helper/script-loader";
import {SENTRY_SCRIPT_CDN} from "../utils/constants";
/**
 * Скрипт расчитан на то, что до его подключения будет установлена переменная window.sentryDsn
 * Если она не установлена - то логирования в sentry не будет
 * @type {boolean}
 */

let useSentryClient = false;
if (typeof window.sentryDsn !== 'undefined' && window.sentryDsn) {
    loadScript(SENTRY_SCRIPT_CDN, function (err) {
        if (err) {
            console.warn('Cannot load sentry');
            return;
        }

        Sentry.init({ dsn: window.sentryDsn });
        useSentryClient = true;
    })

}

/**
 * По умолчанию в клиенте @sentry/browser логируются все ошибки, которые выводятся в пользовательскую консоль
 * Этот метод используется для логирования исключений при ручном перехвате try-catch
 * @param exception
 */
export default function logException(exception) {
    if (useSentryClient) {
        Sentry.captureException(exception);
    }
};

/**
 * Другие подключенные скрипты в рамках документа имеют возможность отлавливать и передавать исключения
 * посредством события captureException
 */
document.addEventListener('captureException', function (event) {
    if (useSentryClient) {
        Sentry.captureException(event.detail.exception);
    }
});
