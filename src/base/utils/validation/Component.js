import {FORM_NAME} from "../../form-validation/constants";

/**
 * Provides class to extend all components from
 */
export class Component {

    constructor(element, app, model) {
        this.element = element;
        this.app = app;
        this.model = model;

        this.currentIndex = 0;
    }

    update(element) {
        // implemented in concrete classes
    }

    getComponentByFullName(fullName) {
        for (let i = 0; i < this.app.components.length; i++) {
            if (this.app.components[i].full_name === fullName) {
                return this.app.components[i];
            }
        }

        return null;
    }

    nextField(current, withOtherField) {
        if (!current.autoTab) {
            return;
        }

        let next = current.element.tabIndex;

        const expireMonthFullName = `${FORM_NAME}.card_exp_month`;
        const expireYearFullName = `${FORM_NAME}.card_exp_year`;

        if ((current.full_name == expireMonthFullName || current.full_name == expireYearFullName) && withOtherField) {
            return
        }

        if (next < this.app.elements.length) {
            this.app.elements[next].focus();
        }

    }

    getValue() {
        return this.element.value;
    }

}
